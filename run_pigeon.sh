flutter pub run pigeon \
  --input pigeons/godex_api.dart \
  --no-dart_null_safety \
  --dart_out lib/src/godex_api.dart \
  --objc_header_out ios/Classes/GodexApi.h \
  --objc_source_out ios/Classes/GodexApi.m \
  --java_out android/src/main/java/tw/omos/flutter_godex_printer/GodexApi.java \
  --java_package "tw.omos.flutter_godex_printer" \