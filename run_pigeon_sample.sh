flutter pub run pigeon \
  --input pigeons/all_types_pigeon.dart \
  --no-dart_null_safety \
  --dart_out lib/src/all_types_pigeon.dart \
  --objc_header_out ios/Classes/AllTypesPigeon.h \
  --objc_source_out ios/Classes/AllTypesPigeon.m \
  --java_out android/src/main/java/tw/omos/flutter_godex_printer/AllTypesPigeon.java \
  --java_package "tw.omos.flutter_godex_printer" \