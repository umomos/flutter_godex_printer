import 'dart:async';
import 'dart:typed_data';

import 'package:ping_discover_network/ping_discover_network.dart';
import 'package:wifi/wifi.dart';

import 'enums.dart';
import 'extension.dart';
import 'godex_api.dart';
import 'godex_printer.dart';

class GodexUtil {
  static final newLinePattern = RegExp(r'(\r\n|\r|\n)');
  static final _api = HostEverything();

  static Future<String> get hostVersion {
    return _api.getHostVersion();
  }

  // 取得 GoDex SDK 的版本號碼。
  static Future<String> get version {
    return _api.getVersion();
  }

  static Future<bool> putImage(Uint8List bitmap, [int px, int py]) {
    return _api.putImage(px ?? 0, py ?? 0, bitmap);
  }

  static Future<Iterable<GodexPrinter>> wifiPrinterSearch() async {
    try {
      final list = await _api.wifiPrinterSearch();
      return (list ?? []).cast<String>().map((e) {
        final array = e.split(newLinePattern);
        return GodexPrinter(
          name: array[0],
          ip: array[1],
          port: array[2],
          macAddress: array[3],
        );
      });
    } catch (e) {
      rethrow;
    }
  }

  static Future<bool> sendCommand(String message, [String code = '']) {
    return _api.sendCommand(message, code ?? '');
  }

  // static Future<bool> writeByte(Uint8List data) {
  //   return _channel.invokeMethod<bool>(WRITE_BYTE, {
  //     'data': data,
  //   });
  // }

  static Future<PrinterStatus> checkStatus() async {
    try {
      final status = await _api.checkStatus();
      return status.trim().printerStatus;
    } catch (e) {
      rethrow;
    }
  }

  static Future<bool> openPort(String address, [OpenType type]) async {
    try {
      return await _api.openPort(address, (type ?? OpenType.Wifi).value);
    } catch (e) {
      throw PrinterStatus.None.name;
    }
    // final completer = Completer<bool>();
    // _api.openPort(address, (type ?? OpenType.Wifi).value).then((value) {
    //   completer.complete(value);
    // }, onError: (error, stackTrace) {
    //   // completer.completeError(error, stackTrace);
    //   completer.completeError(PrinterStatus.None.name);
    // });
    // return completer.future;
  }

  static Future<bool> close() => _api.close();

  static Stream<NetworkAddress> discover({
    int port = 9100,
    Duration timeout = const Duration(milliseconds: 400),
  }) async* {
    final ip = await Wifi.ip;
    final subnet = ip.substring(0, ip.lastIndexOf('.'));
    yield* NetworkAnalyzer.discover2(
      subnet,
      port,
      timeout: timeout ?? Duration(milliseconds: 400),
    );
  }
}

extension _ExtensionString on String {
  PrinterStatus get printerStatus {
    final index =
        PrinterStatus.values.indexWhere((element) => element.value == this);
    if (index >= 0 && index < PrinterStatus.values.length) {
      return PrinterStatus.values.elementAt(index);
    }
    return PrinterStatus.None;
  }
}
