enum ImageDithering {
  None, // 0
  Cluster,
  Dipersed,
  Diffusion,
}

enum DownloadFontID {
  None, // 0
  A,
  B,
  C,
  D,
  E,
  F,
  G,
  H,
  I,
  J,
  K,
  L,
  M,
  N,
  O,
  P,
  Q,
  R,
  S,
  T,
  U,
  V,
  W,
  X,
  Y,
  Z,
}

enum AsiaEncoding {
  BIG5,
  GB2312,
  SJIS,
  EUC_KR,
}

enum AsianFontID {
  Z1,
  Z2,
  Z3,
  Z4,
}

enum InternalFontID {
  A,
  B,
  C,
  D,
  E,
  F,
  G,
  H,
  I,
  J,
}

enum BarGS1Type {
  Omnidir,
  Truncated,
  Stacked,
  Stacked_Omnidir,
  Limited,
  Expanded,
  Expanded_Stacked,
}

enum Readable {
  None,
  Bottom_Left,
  Top_Left,
  Bottom_Centered,
  Top_Centered,
  Bottom_Right,
  Top_Right,
}

class BarCodeType {
  final String value;
  const BarCodeType._internal(this.value);
  static const Code39_Extended = BarCodeType._internal("BA");
  static const Code39_ExtendedCheck = BarCodeType._internal("BA2");
  static const Code39 = BarCodeType._internal("BA3");
  static const Code39_Check = BarCodeType._internal("BA4");
  static const Code39_ExtendedCheckS = BarCodeType._internal("BA5");
  static const Code39_ExtendedS = BarCodeType._internal("BA6");
  static const Logmars = BarCodeType._internal("BA7");
  static const EAN8 = BarCodeType._internal("BB");
  static const EAN8_Add2 = BarCodeType._internal("BC");
  static const EAN8_Add5 = BarCodeType._internal("BD");
  static const EAN13 = BarCodeType._internal("BE");
  static const EAN13_Add2 = BarCodeType._internal("BF");
  static const EAN13_Add5 = BarCodeType._internal("BG");
  static const UPCA = BarCodeType._internal("BH");
  static const UPCA_Add2 = BarCodeType._internal("BI");
  static const UPCA_Add5 = BarCodeType._internal("BJ");
  static const UPCE = BarCodeType._internal("BK");
  static const UPCE_Add2 = BarCodeType._internal("BL");
  static const UPCE_Add5 = BarCodeType._internal("BM");
  static const I2of5 = BarCodeType._internal("BN");
  static const I2of5_CheckDigit = BarCodeType._internal("BN2");
  static const I2of5_CheckDigitNoRead = BarCodeType._internal("BN3");
  static const I2of5_Standard = BarCodeType._internal("BN4");
  static const I2of5_Industrial = BarCodeType._internal("BN5");
  static const Codabar = BarCodeType._internal("BO");
  static const Code93 = BarCodeType._internal("BP");
  static const Code128_Auto = BarCodeType._internal("BQ");
  static const Code128_Subset = BarCodeType._internal("BQ2");
  static const ISBT = BarCodeType._internal("QI");
  static const UCC_128 = BarCodeType._internal("BR");
  static const PostNET = BarCodeType._internal("BS");
  static const Plant11_13 = BarCodeType._internal("BS1");
  static const JPPostnet = BarCodeType._internal("BS2");
  static const ITF14 = BarCodeType._internal("BT");
  static const EAN128 = BarCodeType._internal("BU");
  static const RPS128 = BarCodeType._internal("BV");
  static const HIBC = BarCodeType._internal("BX");
  static const MSI_1MOD10 = BarCodeType._internal("BY");
  static const MSI_2MOD10 = BarCodeType._internal("BY2");
  static const MSI_1MOD1110 = BarCodeType._internal("BY3");
  static const MSI_NoDigitCheck = BarCodeType._internal("BY4");
  static const I2of5_ShippingBearerBars = BarCodeType._internal("BZ");
  static const UCC_EAN128_KMART = BarCodeType._internal("B1");
  static const UCC_EAN128_RANDOM = BarCodeType._internal("B2");
  static const Telepen = BarCodeType._internal("B3");
  static const FIM = BarCodeType._internal("B4");
  static const Plessey = BarCodeType._internal("B7");
  static const GermanPost = BarCodeType._internal("B001");
}

class OpenType {
  final num value;
  const OpenType._internal(this.value);
  static const Wifi = OpenType._internal(1);
  static const BTH = OpenType._internal(2);
  static const USB = OpenType._internal(3);
  static const BLE = OpenType._internal(4);
}

class PrinterStatus {
  final String value;
  const PrinterStatus._internal(this.value);
  static const None = PrinterStatus._internal('--');
  static const Okay = PrinterStatus._internal('00');
  static const Error01 = PrinterStatus._internal('01');
  static const Error02 = PrinterStatus._internal('02');
  static const Error03 = PrinterStatus._internal('03');
  static const Error04 = PrinterStatus._internal('04');
  static const Error05 = PrinterStatus._internal('05');
  static const Error06 = PrinterStatus._internal('06');
  static const Error07 = PrinterStatus._internal('07');
  static const Error08 = PrinterStatus._internal('08');
  static const Error09 = PrinterStatus._internal('09');
  static const Error10 = PrinterStatus._internal('10');
  static const Error11 = PrinterStatus._internal('11');
  static const Error20 = PrinterStatus._internal('20');
  static const Error21 = PrinterStatus._internal('21');
  static const Error22 = PrinterStatus._internal('22');
  static const Error50 = PrinterStatus._internal('50');
  static const Error60 = PrinterStatus._internal('60');
  static final values = [
    None,
    Okay,
    Error01,
    Error02,
    Error03,
    Error04,
    Error05,
    Error06,
    Error07,
    Error08,
    Error09,
    Error10,
    Error11,
    Error20,
    Error21,
    Error22,
    Error50,
    Error60,
  ];
}
