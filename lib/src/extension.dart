import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:ping_discover_network/ping_discover_network.dart';

import 'enums.dart';
import 'godex_printer.dart';
import 'godex_util.dart';

extension ExtensionGodexPrinter on GodexPrinter {
  static final _duration = Duration(milliseconds: 200);
  static final _delay = Future.delayed(_duration);

  Future<bool> _openPort([OpenType type]) async {
    try {
      final ret = await GodexUtil.openPort(ip, type);
      await _delay;
      return ret;
    } catch (e) {
      rethrow;
    }
  }

  Future<bool> _close() async {
    try {
      final ret = await GodexUtil.close();
      await _delay;
      return ret;
    } catch (e) {
      rethrow;
    }
  }

  Future<T> execute<T>(AsyncValueGetter<T> getter) async {
    try {
      T result;
      if (true == await _openPort()) {
        result = await getter.call();
      }
      return result;
    } catch (e) {
      rethrow;
    } finally {
      await _close();
    }
  }

  Future<PrinterStatus> get checkStatus {
    return execute<PrinterStatus>(() => GodexUtil.checkStatus());
  }

  // Future<bool> writeByte(Uint8List data) => GodexUtil.writeByte(data);

  Future<bool> _sendCommand(String message, [String code]) async {
    try {
      final ret = await GodexUtil.sendCommand(message, code);
      await _delay;
      return ret;
    } catch (e) {
      rethrow;
    }
  }

  // 重置
  Future<bool> reset() {
    return execute<bool>(() => _sendCommand('~Z'));
  }

  // 自動校正
  Future<bool> autoCalibrate() {
    return execute<bool>(() async {
      var result = true;
      if (false == await _sendCommand('~S,SENSOR')) {
        result = false;
      }
      if (false == await _sendCommand('^W42')) {
        result = false;
      }
      return result;
    });
  }

  // 設定印表機名稱
  Future<bool> setPrinterAliasName(String name) {
    return execute(() => _sendCommand('^XSET,ALIAS,$name'));
  }

  // 列印多張圖片
  // Future<bool> printImage(Uint8List bitmap) {
  //   return execute<bool>(() => putImage(bitmap));
  // }

  // 列印多張圖片
  Future<bool> printImages(Iterable<List<int>> images) {
    return execute<bool>(() async {
      for (var image in images) {
        if (false == await putImage(image)) {
          return false;
        }
        await Future.delayed(Duration(milliseconds: 600));
      }
      return true;
    });
  }

  // 放置一張圖片
  Future<bool> putImage(Uint8List bitmap) async {
    if (false == await _sendCommand('^L')) {
      return false;
    }
    if (false == await GodexUtil.putImage(bitmap)) {
      return false;
    }
    await _delay;
    if (false == await _sendCommand('E')) {
      return false;
    }
    return true;
  }
}

extension ExtensionPrinterStatus on PrinterStatus {
  String get name {
    switch (this) {
      case PrinterStatus.None:
        return '無法連線標籤機';
      case PrinterStatus.Okay:
        return '待機狀態';
      case PrinterStatus.Error01:
        return '耗材用盡或紙張偵測錯誤';
      case PrinterStatus.Error02:
        return '耗材用盡或紙張偵測錯誤';
      case PrinterStatus.Error03:
        return '碳帶用盡';
      case PrinterStatus.Error04:
        return '印表頭開啟';
      case PrinterStatus.Error05:
        return '背紙回收器已滿';
      case PrinterStatus.Error06:
        return '檔案系統已滿';
      case PrinterStatus.Error07:
        return '找不到檔案';
      case PrinterStatus.Error08:
        return '檔名重複';
      case PrinterStatus.Error09:
        return '指令語法錯誤';
      case PrinterStatus.Error10:
        return '裁刀卡住或未安裝裁刀';
      case PrinterStatus.Error11:
        return '無延伸記憶體';
      case PrinterStatus.Error20:
        return '暫停';
      case PrinterStatus.Error21:
        return '設定模式';
      case PrinterStatus.Error22:
        return '鍵盤模式';
      case PrinterStatus.Error50:
        return '印表機列印中';
      case PrinterStatus.Error60:
        return '資料處理中';
      default:
        return '';
    }
  }
}

extension NetworkAddressX on NetworkAddress {
  GodexPrinter toGodexPrinter() {
    return GodexPrinter(
      name: name,
      ip: ip,
      port: port,
      macAddress: '',
    );
  }

  String get name {
    return '$ip:$port';
  }

  String get port => '9100';
}
