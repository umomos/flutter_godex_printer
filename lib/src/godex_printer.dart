// To parse this JSON data, do
//
//     final godexPrinter = godexPrinterFromJson(jsonString);

import 'dart:convert';

class GodexPrinter {
  GodexPrinter({
    this.name,
    this.ip,
    this.port,
    this.macAddress,
  });

  String name;
  String ip;
  String port;
  String macAddress;

  GodexPrinter copyWith({
    String name,
    String ip,
    String port,
    String macAddress,
  }) =>
      GodexPrinter(
        name: name ?? this.name,
        ip: ip ?? this.ip,
        port: port ?? this.port,
        macAddress: macAddress ?? this.macAddress,
      );

  factory GodexPrinter.fromRawJson(String str) =>
      GodexPrinter.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GodexPrinter.fromJson(Map<String, dynamic> json) => GodexPrinter(
        name: json["name"] == null ? null : json["name"],
        ip: json["ip"] == null ? null : json["ip"],
        port: json["port"] == null ? null : json["port"],
        macAddress: json["mac_address"] == null ? null : json["mac_address"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "ip": ip == null ? null : ip,
        "port": port == null ? null : port,
        "mac_address": macAddress == null ? null : macAddress,
      };
}
