import 'package:flutter_godex_printer/flutter_godex_printer.dart';
import 'package:flutter_godex_printer/src/godex_util.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const input = "DT2x2\n192.168.1.126\n9100\n00:1D:9A:09:95:7C";
  test('GodexPrinter', () {
    final array = input.split(GodexUtil.newLinePattern);
    final printer = GodexPrinter(
      name: array[0],
      ip: array[1],
      port: array[2],
      macAddress: array[3],
    );
    expect(printer.name, array[0]);
    expect(printer.ip, array[1]);
    expect(printer.port, array[2]);
    expect(printer.macAddress, array[3]);
  });
}
