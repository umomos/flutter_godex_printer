import Flutter
import UIKit
import GoDEXSDK

public class SwiftFlutterGodexPrinterPlugin: NSObject, FlutterPlugin, HostEverything {
    typealias CompletionBlock = (_ call: FlutterMethodCall, _ result: @escaping FlutterResult ) -> Void
    // let completion: CompletionBlock = { call, result in print(result) }
    
    static let GET_PLATFORM_VERSION = "getPlatformVersion"
    static let WIFI_PRINTER_SEARCH = "wifi_printer_search"
    static let SEND_COMMAND = "send_command"
    static let WRITE_BYTE = "write_byte"
    static let OPEN_PORT = "open_port"
    static let CLOSE = "close"
    static let SETUP = "setup"
    static let DEBUG = "debug"
    static let READ = "read"
    static let READ_BYTE = "read_byte"
    static let PRINT_CMD_FILE = "print_cmd_file"
    static let LOAD_IMAGE = "load_image"
    static let PUT_IMAGE = "put_image"
    // static let PUT_IMAGE = "Bar_1D"
    // static let PUT_IMAGE = "Bar_GS1Databar"
    // static let PUT_IMAGE = "Bar_PDF147"
    // static let PUT_IMAGE = "Bar_Maxicode"
    // static let PUT_IMAGE = "Bar_DataMatrix"
    // static let PUT_IMAGE = "Bar_QRCode"
    // static let PUT_IMAGE = "Bar_Aztec"
    static let CHECK_STATUS = "check_status"
    // static let PUT_IMAGE = "InternalFont_TextOut"
    // static let PUT_IMAGE = "AsiaFont_TextOut"
    // static let PUT_IMAGE = "SoftFont_TextOut"
    // static let PUT_IMAGE = "TrueTypeFont_TextOut"
    // static let PUT_IMAGE = "ecTextOut"
    // static let PUT_IMAGE = "ecTextDownload"
    // static let PUT_IMAGE = "ecTextDownload2"
    // static let PUT_IMAGE = "ecTextOutWordWrap"
    // static let PUT_IMAGE = "ecTextOutWordWrap"
    // static let PUT_IMAGE = "getPrinterDPI"
    static let GET_VERSION = "getVersion"
    static let GET_VERSION_2 = "getVersion2"
    static let GET_VERSION_3 = "getVersion3"

    let godex = GoDEX()
    private var actions = [String: CompletionBlock]();
        
    private func getVersion(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        result(godex.GetVersion())
    }
    
    let getVersion2 = getVersion;
    
    let getVersion3 = { (_ call: FlutterMethodCall, _ result: @escaping FlutterResult) -> Void in
        //result(godex.GetVersion())
    }
    
    override init() {
        super.init()
        /* 紀錄，不使用
        actions[SwiftFlutterGodexPrinterPlugin.GET_VERSION] = getVersion
        //actions[SwiftFlutterGodexPrinterPlugin.GET_VERSION_2] = getVersion2
        actions[SwiftFlutterGodexPrinterPlugin.GET_VERSION_3] = getVersion3
        actions[SwiftFlutterGodexPrinterPlugin.WIFI_PRINTER_SEARCH] = _wifiPrinterSearch
        actions[SwiftFlutterGodexPrinterPlugin.SEND_COMMAND] = _sendCommand
        actions[SwiftFlutterGodexPrinterPlugin.WRITE_BYTE] = writeByte
        actions[SwiftFlutterGodexPrinterPlugin.OPEN_PORT] = _openPort
        actions[SwiftFlutterGodexPrinterPlugin.CLOSE] = _close
        actions[SwiftFlutterGodexPrinterPlugin.SETUP] = setup
        actions[SwiftFlutterGodexPrinterPlugin.DEBUG] = _debug
        actions[SwiftFlutterGodexPrinterPlugin.READ] = read
        actions[SwiftFlutterGodexPrinterPlugin.READ_BYTE] = readByte
        actions[SwiftFlutterGodexPrinterPlugin.PRINT_CMD_FILE] = printCmdFile
        actions[SwiftFlutterGodexPrinterPlugin.LOAD_IMAGE] = loadImage
        actions[SwiftFlutterGodexPrinterPlugin.PUT_IMAGE] = _putImage
        actions[SwiftFlutterGodexPrinterPlugin.CHECK_STATUS] = _checkStatus
        actions[SwiftFlutterGodexPrinterPlugin.GET_PLATFORM_VERSION] = _getPlatFormVersion
         */
    }
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let messenger: FlutterBinaryMessenger = registrar.messenger()
        let api: HostEverything & NSObjectProtocol = SwiftFlutterGodexPrinterPlugin.init()
        HostEverythingSetup(messenger, api)
//        let channel = FlutterMethodChannel(name: "flutter_godex_printer", binaryMessenger: registrar.messenger())
//        let instance = SwiftFlutterGodexPrinterPlugin()
//        registrar.addMethodCallDelegate(instance, channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if let action = actions[call.method] {
            action(call, result);
        } else {
            result(FlutterMethodNotImplemented)
        }
    }
    
    private func _wifiPrinterSearch(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        let ret = godex.findAllNet()
        let strings = ret?.map { (element) -> String in
            let ls = element as! [String]
            return ls.joined(separator: "\r\n")
        }
        result(strings);
    }

    private func _sendCommand(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        guard let args = call.arguments as? [String : Any],
              let data: String = args["command"] as? String
        else {
            result(false)
            return
        }
        result(godex.sendCommand(Command: data))
    }

    private func writeByte(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        result(FlutterMethodNotImplemented)
    }
    
    private func _openPort(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        // if let args = call.arguments as? Dictionary<String, Any>
        guard let args = call.arguments as? [String : Any],
              let address: String = args["address"] as? String
        else {
            result(false)
            return
        }
        result(godex.openport(address: address, port: 9100))
    }

    private func _close(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        godex.close()
        result(true)
    }

    private func setup(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        result(FlutterMethodNotImplemented)
    }

    private func _debug(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        guard let args = call.arguments as? [String : Any],
              let select: Int = args["select"] as? Int
        else {
            result(false)
            return
        }
        godex.debug(Select: select)
    }

    private func read(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        result(FlutterMethodNotImplemented)
    }

    private func readByte(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        result(FlutterMethodNotImplemented)
    }

    private func printCmdFile(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        result(FlutterMethodNotImplemented)
    }

    private func loadImage(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        result(FlutterMethodNotImplemented)
    }
    
    private func _putImage(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        guard let args = call.arguments as? [String : Any],
                let px: Int = args["px"] as? Int,
                let py: Int = args["py"] as? Int,
                let bytes: FlutterStandardTypedData = args["bitmapData"] as? FlutterStandardTypedData
                // let length: Int = args["dataLength"] as? Int
        else {
            result(false)
            return
        }
        
        //let data = Data(bytes: bytes.data, count: length)
        let data = Data(bytes.data)
        let image = UIImage(data: data)!
        result(godex.putimage(PosX: px, PosY: py, Image: image))
    }

    private func _checkStatus(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        godex.read()
        result("00")
    }
    
    private func _getPlatFormVersion(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        result("iOS " + UIDevice.current.systemVersion)
    }
    
    public func getHostVersionWithError(_ error: AutoreleasingUnsafeMutablePointer<FlutterError?>) -> String? {
        return "iOS " + UIDevice.current.systemVersion
    }
    
    public func getVersionWithCompletion(_ completion: @escaping (String?, FlutterError?) -> Void) {
        // DispatchQueue.global().async {
            let ret = self.godex.GetVersion()
            completion(ret, nil)
        // }
    }
    
    public func wifiPrinterSearch(completion: @escaping ([Any]?, FlutterError?) -> Void) {
        DispatchQueue.global().async {
            let ret = self.godex.findAllNet() ?? []
            let strings = ret.map { (element) -> String in
                let ls = element as! [String]
                return ls.joined(separator: "\r\n")
            }
            DispatchQueue.main.async {
                completion(strings, nil)
            }
        }
    }
    
    public func openPortAddress(_ address: String?, type: NSNumber?, completion: @escaping (NSNumber?, FlutterError?) -> Void) {
        // DispatchQueue.global().async {
            let ret = self.godex.openport(address: address!, port: 9100)
            completion(ret as NSNumber, nil)
        // }
    }
    
    public func sendCommandMessage(_ message: String?, code: String?, completion: @escaping (NSNumber?, FlutterError?) -> Void) {
        // DispatchQueue.global().async {
            let ret = self.godex.sendCommand(Command: message!)
            completion(ret as NSNumber, nil)
        // }
    }
    
    public func read(completion: @escaping (String?, FlutterError?) -> Void) {
        // DispatchQueue.global().async {
            completion(self.godex.read(), nil)
        // }
    }
    
    public func putImagePx(_ px: NSNumber?, py: NSNumber?, bitmap: FlutterStandardTypedData?, completion: @escaping (NSNumber?, FlutterError?) -> Void) {
        let data = Data(bitmap!.data)
        let image = UIImage(data: data)!
        let ret = self.godex.putimage(Image: image)
        completion(ret as NSNumber, nil);
//        DispatchQueue.global().async {
//            let data = Data(bitmap!.data)
//            let image = UIImage(data: data)!
//            DispatchQueue.main.async {
//                let ret = self.godex.putimage(Image: image)
//                completion(ret as NSNumber, nil);
//            }
//        }
    }
    
    public func close(completion: @escaping (NSNumber?, FlutterError?) -> Void) {
        // DispatchQueue.global().async {
            self.godex.close();
            completion(true, nil)
        // }
    }
    
    public func checkStatus(completion: @escaping (String?, FlutterError?) -> Void) {
        // DispatchQueue.global().async {
            if (self.godex.sendCommand(Command: "~S,Check")) {
                let ret = self.godex.read() ?? ""
                completion(ret.isEmpty ? "00" : ret, nil);
            } else {
                completion("00", nil)
            }
        // }
    }
}
