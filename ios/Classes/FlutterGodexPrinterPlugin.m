#import "FlutterGodexPrinterPlugin.h"
#if __has_include(<flutter_godex_printer/flutter_godex_printer-Swift.h>)
#import <flutter_godex_printer/flutter_godex_printer-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_godex_printer-Swift.h"
#endif

@implementation FlutterGodexPrinterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterGodexPrinterPlugin registerWithRegistrar:registrar];
}
@end
