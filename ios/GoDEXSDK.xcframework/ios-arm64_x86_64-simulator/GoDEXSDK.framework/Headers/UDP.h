//
//  UDP.h
//  udp
//
//  Created by SF639 on 2015/4/2.
//  Copyright (c) 2015年 GoDEX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>

@interface UDP : NSObject
- (BOOL)UPDBroadcast;
- (NSMutableArray *)getPrinterInfo;
@end
