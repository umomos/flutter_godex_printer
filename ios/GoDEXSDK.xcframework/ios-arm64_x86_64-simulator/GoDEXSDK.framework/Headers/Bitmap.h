//
//  Bitmap.h
//  NSStreamSample
//
//  Created by Ming on 2015/11/2.
//  Copyright © 2015年 Ming. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef struct ARGBPixel {
    unsigned char a;
    unsigned char r;
    unsigned char g;
    unsigned char b;
} ARGBPixel;

typedef struct RGBPixel {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} RGBPixel;

#define BITS_PER_COMPONENT 8
#define _32_BYTE_PER_PIXEL 4
#define _24_BYTE_PER_PIXEL 3

typedef NS_ENUM(NSInteger, ImageDithering) {
    None,
    Cluster,
    Dipersed,
    Diffusion,
};

@interface Bitmap : NSObject

@property (nonatomic)NSInteger width;
@property (nonatomic)NSInteger height;
@property (nonatomic)NSInteger filesize;


//- (NSData *)putImage:(UIImage *)image pointX:(NSInteger)px pointY:(NSInteger)py;
- (NSData *)putImage:(UIImage *)image pointX:(NSInteger)x pointY:(NSInteger)y dihter:(ImageDithering)dither;
- (NSData *)loadImage:(UIImage *)image dither:(ImageDithering)dither;
@end
