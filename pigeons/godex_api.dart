import 'package:pigeon/pigeon.dart';

/// Flutter调用原生的方法
@HostApi()
abstract class HostEverything {
  String getHostVersion();
  @async
  String getVersion();
  @async
  List wifiPrinterSearch();
  @async
  bool openPort(String address, int type);
  @async
  bool sendCommand(String message, String code);
  @async
  String read();
  @async
  bool putImage(int px, int py, Uint8List bitmap);
  @async
  String checkStatus();
  @async
  bool close();
}
