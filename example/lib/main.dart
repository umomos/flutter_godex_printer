import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_godex_printer/flutter_godex_printer.dart';

import 'refresh_data_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  // Future _calculation = FlutterGodexPrinter.wifiPrinterSearch();
  Future<Iterable<String>> _calculation;
  final items = <String>[];

  @override
  void initState() {
    super.initState();
    // initPlatformState();
    // _calculation = FlutterGodexPrinter.wifiPrinterSearch();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await GodexUtil.hostVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        // body: RefreshDataPage(),
        body: RefreshIndicator(
          onRefresh: () async {
            final data = await GodexUtil.wifiPrinterSearch();
            setState(() {
              // _calculation = Future.value(data);
            });
          },
          child: FutureBuilder<Iterable<GodexPrinter>>(
            // future: _calculation,
            future: GodexUtil.wifiPrinterSearch(),
            builder: (context, snapshot) {
              // print('$snapshot');
              if (snapshot.hasError) {
                print('has error');
                return Text('${snapshot.error}');
              }
              if (snapshot.hasData) {
                final it = snapshot.data;
                return ListView.separated(
                  physics: const AlwaysScrollableScrollPhysics(),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    final element = it.elementAt(index);
                    return TextButton(
                      child: Text(element.toRawJson() ?? ''),
                      onPressed: () {
                        _checkStatus(element.ip);
                        // _image(element.ip);
                      },
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider(
                      height: 1,
                    );
                  },
                );
              }
              print('loading');
              return Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        ),
        // body1: Center(
        //   child: Text('Running on: $_platformVersion\n'),
        // ),
      ),
    );
  }

  Future<void> _checkStatus(String ip) async {
    final printer = GodexPrinter(ip: ip);
    final status = await printer.checkStatus;
  }

  Future<void> _image(String ip) async {
    final printer = GodexPrinter(ip: ip);
    final data = <int>[1, 2, 4];
    final bitmap = Uint8List.fromList([1, 2, 3]);
    // final status = await printer.printImage(bitmap);
    await printer.execute(() async {
      if (false == await GodexUtil.putImage(bitmap)) {
        return false;
      }
      if (false == await GodexUtil.putImage(bitmap)) {
        return false;
      }
      return true;
    });
  }
}
