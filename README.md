# flutter_godex_printer

A new flutter plugin project.

## Getting Started

```zsh
flutter create --org tw.omos --template=plugin --platforms=android,ios -a java -i swift flutter_godex_printer
```

## [ios plugin 加入 xcframework 的方式](https://stackoverflow.com/a/70210039)

## [pigeon](https://pub.dev/packages/pigeon/)

* [Flutter官方推荐插件开发辅助工具-Pigeon](https://juejin.cn/post/7007724838496239630)
* [example](https://github.com/flutter/packages/tree/main/packages/pigeon/example)

### 流程

1. 編寫 pigeons/all_types_pigeon.dart
2. 執行 run_pigeon_sample.sh

## 問題排除

### The iOS deployment target ‘IPHONEOS_DEPLOYMENT_TARGET’ is set to 8.0, but the range of supported deployment target versions is 9.0 to 14.4.99. (in target ‘FirebaseInstallations’ from project ‘Pods’)

* [IPHONEOS_DEPLOYMENT_TARGET](https://medium.com/%E5%BD%BC%E5%BE%97%E6%BD%98%E7%9A%84-swift-ios-app-%E9%96%8B%E7%99%BC%E6%95%99%E5%AE%A4/the-ios-deployment-target-iphoneos-deployment-target-is-set-to-8-0-140c322057e7)
* [ios multi thread](https://ithelp.ithome.com.tw/articles/10204233)
