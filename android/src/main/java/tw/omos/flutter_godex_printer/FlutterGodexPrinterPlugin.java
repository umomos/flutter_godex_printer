package tw.omos.flutter_godex_printer;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.godex.Godex;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * FlutterGodexPrinterPlugin
 */
public class FlutterGodexPrinterPlugin implements FlutterPlugin, MethodCallHandler, GodexApi.HostEverything {
    @SuppressLint("CheckResult")
    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        GodexApi.HostEverything.setup(flutterPluginBinding.getBinaryMessenger(), this);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        GodexApi.HostEverything.setup(binding.getBinaryMessenger(), null);
    }

    @Override
    public String getHostVersion() {
        return "Android " + android.os.Build.VERSION.RELEASE;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getVersion(GodexApi.Result<String> result) {
        Observable.fromCallable(Godex::getVersion)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result::success, result::error);
    }

    @SuppressLint("CheckResult")
    @Override
    public void wifiPrinterSearch(GodexApi.Result<List<Object>> result) {
        Observable.fromCallable(Godex::WiFiPrinterSearch)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((event) -> {
                    result.success(new ArrayList<>(event));
                }, (throwable) -> {
                    result.error(throwable);
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void openPort(String address, Long type, GodexApi.Result<Boolean> result) {
        Observable.fromCallable(() -> Godex.openport(address, type.intValue()))
                .timeout(3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result::success, result::error);
    }

    @SuppressLint("CheckResult")
    @Override
    public void sendCommand(String message, String code, GodexApi.Result<Boolean> result) {
        Observable.fromCallable(() -> {
            if (TextUtils.isEmpty(code)) {
                return Godex.sendCommand(message);
            } else {
                return Godex.sendCommand(message, code);
            }
        })
                .timeout(3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result::success, result::error);
    }

    @SuppressLint("CheckResult")
    @Override
    public void read(GodexApi.Result<String> result) {
        Observable.fromCallable(() -> {
            String[] status = new String[1];
            Godex.read(status);
            return status[0];
        })
                .timeout(3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result::success, result::error);
    }

    @SuppressLint("CheckResult")
    @Override
    public void putImage(Long px, Long py, byte[] bitmap, GodexApi.Result<Boolean> result) {
        Observable.fromCallable(() -> {
            final Bitmap bm = BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length);
            return Godex.putImage(px.intValue(), py.intValue(), bm);
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result::success, result::error);
    }

    @SuppressLint("CheckResult")
    @Override
    public void checkStatus(GodexApi.Result<String> result) {
        Observable.fromCallable(Godex::CheckStatus)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result::success, result::error);
    }

    @SuppressLint("CheckResult")
    @Override
    public void close(GodexApi.Result<Boolean> result) {
        Observable.fromCallable(Godex::close)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result::success, result::error);
    }
}
