package tw.omos.flutter_godex_printer;

public interface BiConsumer<@NonNull T1, @NonNull T2> {
    /**
     * Performs an operation on the given values.
     *
     * @param t1 the first value
     * @param t2 the second value
     * @throws Throwable if the implementation wishes to throw any type of exception
     */
    void accept(T1 t1, T2 t2) throws Throwable;
}
